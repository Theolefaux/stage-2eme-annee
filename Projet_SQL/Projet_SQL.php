<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projet_SQL</title>
    
    <style>
        body {
            background-color: rgb(240, 240, 240);
            height: 100%;
            width: 100%;
            overflow: hidden;
        }

        /* Navbar/Entête + différentes sections de la page */

        header {
            overflow: hidden;
            background-color:#222222;
            position: fixed;
            top: 0; left: 0;
            height: 15vh;
            width: 100%;
            text-align: center;
            color: white;
        }

        .UDP-LOGO {
            height: 80px;
            position: relative;
            top: 0rem;
            left: -10rem;
        }

        .title-project {
            text-align: end;
            padding-right: 10vh;
        }

        section {
            padding-top: 20vh;
        }

        /* Container de la page */

        .container-principal {
            background-color:#222222;
            width: 85rem;
            margin-left: auto;
            margin-right: auto;
            border-radius: 10px;
        }

        .container-requete {
            width: 85rem;
            margin-left: auto;
            margin-right: auto;
        }

        .container-infos {
            width: 85rem;
            padding-top: 2rem;
            margin-left: auto;
            margin-right: auto;
        }


        /* Mise en forme en lignes et colonnes */

        .row {
            display: flex;
        }

        .col {
            flex: 1; 
            padding: 1em;
        }

        .zone-texte-SQL {
            justify-content: center;
        }

        .requete-SQL {
            padding-top: 2vh;
        }

        .requete-message {
            padding-top: 2vh;
        }


        /* Input */

        .textarea {
            background-color: #dddddd;
            color: #666666;
            padding: 1em;
            border-radius: 10px;
            border: 2px solid transparent; 
            outline: none;
            font-family: 'Heebo', sans-serif;
            font-weight: 100;
            font-size: 16px;
            line-height: 1.4;
            width: 300px;
            height: 150px;
            transition: all 0.2s;
        }

        .textarea:hover {
            cursor: pointer;
            background-color: #eeeeee;
        }

        .textarea:focus {
            cursor: text;
            color: #333333;
            background-color: white;
            border-color: #333333;
        }

        .textarea-title {
            color: #fff;
        }

        .form__group {
            position: relative;
            padding: 15px 0 0;
            margin-top: 10px;
            width: 50%;
        }
        
        .form__field {
            font-family: inherit;
            width: 100%;
            border: 0;
            border-bottom: 2px solid #9b9b9b;
            outline: 0;
            font-size: 1.3rem;
            color: #fff;
            padding: 7px 0;
            background: transparent;
            transition: border-color 0.2s;
        }
        
        .form__label {
            position: absolute;
            top: 0;
            display: block;
            transition: 0.2s;
            font-size: 1rem;
            color: #9b9b9b;
        }
        
        .form__field:focus {
            padding-bottom: 6px;  
            font-weight: 700;
            border-width: 3px;
            border-image: linear-gradient(to right, #628ac7, #1539af);
            border-image-slice: 1;
        }

        /* Bouton Requetage */

        .btn-clear-storage {
            margin-top: 4vh;
            border: none;
            padding: 10px;
            border-radius: 8px;
            background-color: rgb(202, 40, 40);
            color: #fff;
        }

        .btn-clear-storage:hover {
            background-color: rgb(165, 33, 33);
            color: #fff;
            cursor: pointer;
        }

        .btn-data {
            margin-top: 14vh;
            background-color: #11998e;
            border-radius: 10px;
            border: none;
            padding: 10px 14px 10px 14px;
            color: aliceblue;
            cursor: pointer;
        }

        .btn-data:hover {
            background-color: #0d7a71;
            color: #fff;
            cursor: pointer;
        }

        .input-XML {
            text-align: center;
            padding: 5vh;
        }

        .input-text-XML {
            text-align: -moz-center;
        }
    </style>
</head>
<body>
    <header>
        <div class="row">
            <div class="col">
                <img src="UDP_Logo.png" alt="" class="UDP-LOGO">
            </div>
            <div class="col">
                <h1 class="title-project">GOUVERNEUR Théo : SIO2</h1>
            </div>
        </div>
    </header>
    <section>
        <form action="Projet_SQL_2.php" method="POST">
            <div class="container-principal">
                <div class="container-requete">
                    <div class="row zone-texte-SQL">
                        <div class="col input-XML">
                            <p class="requete-SQL">
                                <h3 class="textarea-title">
                                    Quelle requête souhaitez-vous rentrer ?       
                                </h3> 
                                <textarea class="requete-SQL textarea" name="requete_sql" id="requete_sql" rows="10" cols="50"></textarea>      
                            </p>
                        </div>
                        <div class="input-XML">
                            <div>
                                <input class="btn-data" id="data" name="submit" onclick="window.location.href = 'Projet_SQL_2.php'" type="submit" value="Envoyer les datas">
                            </div>
                            <div>
                                <form action="Projet_SQL_Supr.php" method="post">
                                    <input class="btn-clear-storage" id="clear" name="reset" type="submit" value="Effacer le stockage">
                                </form>
                            </div>
                        </div>
                        <div class="col input-XML">
                            <p class="requete-message">
                                <h3 class="textarea-title">
                                    Quel message souhaitez vous insérer ?       
                                </h3>
                                <textarea class="requete-message textarea" name="requete_message" id="requete_message" rows="10" cols="50"></textarea> 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="container-infos">
                    <div class="row">
                        <div class="col input-text-XML">
                            <div class="form__group field">
                                <input type="text" class="form__field" name="caption" id='caption' required />
                                <label class="form__label">Caption :</label>
                            </div>
                        </div>
                        <div class="col input-text-XML">
                            <div class="form__group field">
                                <input type="text" class="form__field" name="description" id='description' required />
                                <label class="form__label">Description :</label>
                            </div>
                        </div>
                        <div class="col input-text-XML">
                            <div class="form__group field">
                                <input type="text" class="form__field" name="label" id='label' required />
                                <label class="form__label">Label :</label>
                            </div>
                        </div>
                        <div class="col input-text-XML">
                            <div class="form__group field">
                                <input type="number" class="form__field" name="StartValue" id='StartValue' required />
                                <label class="form__label">Start Value :</label>
                            </div>
                        </div>
                        <div class="col input-text-XML">
                            <div class="form__group field">
                                <input type="number" class="form__field" name="EndValue" id='EndValue' required />
                                <label class="form__label">End Value :</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <footer>

    </footer>
</body>
</html>

<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Récupérer les valeurs des inputs du formulaire
if($SERVER['REQUEST_METHOD'] == 'POST') {
    $caption = $_POST['caption'];
    $description = $_POST['description'];
    $requete_message = $_POST['requete_message'];
    $requete_sql = $_POST['requete_sql'];
    $label = $_POST['label'];
    $StartValue = $_POST['StartValue'];
    $EndValue = $_POST['EndValue'];
}

?>