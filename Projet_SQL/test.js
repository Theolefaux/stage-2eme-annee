var data_requete_sql;
var data_requete_message;

var data_descr;
var data_label;
var data_StartValue;  
var data_EndValue;  

function requete_sql_xml() {
    let requete_sql = document.getElementById("requete-SQL").value;
    console.log(requete_sql);
    data_requete_sql = localStorage.setItem('requete_sql', data_requete_sql);
    data_requete_sql = localStorage.getItem('requete_sql', requete_sql.value);
  }
  
  function requete_message_xml() {
    let requete_message = document.getElementById("requete-message").value;
    console.log(requete_message);
    data_requete_message = localStorage.setItem('requete_message', requete_message.value);
  }
  
  function data_xml() {
  
    /* Data nom description */
    let description_xml = document.getElementById("Description-XML").value;
    data_descr = localStorage.setItem('Description-XML', data_descr);
    console.log(description_xml);
  
    /* Data nom label */
    let label_XML = document.getElementById("Label-XML").value;
    data_label = localStorage.setItem('Label-XML', data_label);
    console.log(label_XML);
  
    /* Data nom Valeur de début */
    let StartValue_XML = document.getElementById("StartValue-XML").value;
    data_StartValue = localStorage.setItem('StartValue-XML', data_StartValue);
    console.log(StartValue_XML);
  
    /* Data nom Valeur de fin */
    let EndValue_XML = document.getElementById("EndValue-XML").value;
    data_EndValue = localStorage.setItem('EndValue-XML', data_EndValue);
    console.log(EndValue_XML);
  }
  
  function clear_storage() {
    localStorage.clear();
    document.location.reload();
  }